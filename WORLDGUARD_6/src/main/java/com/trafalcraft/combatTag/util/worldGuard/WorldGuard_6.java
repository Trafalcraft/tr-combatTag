package com.trafalcraft.combatTag.util.worldGuard;

import java.util.Objects;

import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

class WorldGuard_6 implements IWorldGuard {

    public WorldGuard_6 () {}

    @Override
    public boolean pvpNotAllowed(Player p) {
        return !checkFlag(p, DefaultFlag.PVP);
    }

    @Override
    public boolean mobDamageNotAllowed(Player p) {
        return !checkFlag(p, DefaultFlag.MOB_DAMAGE);
	}

    private boolean checkFlag(Player p, StateFlag flag) {
        LocalPlayer lp = Objects.requireNonNull(getWorldGuard()).wrapPlayer(p);
        ApplicableRegionSet set = Objects.requireNonNull(getWorldGuard()).getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation());
        return set.testState(lp, flag);
    }

    private static WorldGuardPlugin getWorldGuard() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");

        // WorldGuard may not be loaded
        if (!(plugin instanceof WorldGuardPlugin)) {
            return null; // Maybe you want throw an exception instead
        }

        return (WorldGuardPlugin) plugin;
    }

}
