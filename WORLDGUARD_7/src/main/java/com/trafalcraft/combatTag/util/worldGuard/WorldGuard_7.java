package com.trafalcraft.combatTag.util.worldGuard;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

class WorldGuard_7 implements IWorldGuard {

    public WorldGuard_7 () {}

    @Override
    public boolean pvpNotAllowed(Player p) {
        return !checkFlag(p, Flags.PVP);
    }

    @Override
    public boolean mobDamageNotAllowed(Player p) {
        return !checkFlag(p, Flags.MOB_DAMAGE);
	}

    private boolean checkFlag(Player p, StateFlag flag) {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
        if (!(plugin instanceof WorldGuardPlugin)) {
            return false; // Maybe you want throw an exception instead
        }
        WorldGuardPlugin wg = (WorldGuardPlugin) plugin;

        LocalPlayer lp = wg.wrapPlayer(p);
        BlockVector3 wgVector = BukkitAdapter.asBlockVector(p.getLocation());
        ApplicableRegionSet set = WorldGuard.getInstance().getPlatform().getRegionContainer()
            .get(WorldGuard.getInstance().getPlatform().getMatcher().getWorldByName(p.getWorld().getName()))
            .getApplicableRegions(wgVector);
        return set.testState(lp, flag);
    }
}
