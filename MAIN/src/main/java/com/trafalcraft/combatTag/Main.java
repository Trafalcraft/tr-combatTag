package com.trafalcraft.combatTag;

import com.trafalcraft.combatTag.object.PlayerTag;
import com.trafalcraft.combatTag.object.PlayerTagController;
import com.trafalcraft.combatTag.util.worldGuard.VersionWG;
import com.trafalcraft.combatTag.util.worldGuard.IWorldGuard;

import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static PlayerTagController ptc;
    private static JavaPlugin plugin;
    private static IWorldGuard worldGuard;
    private static boolean useBossBar;
    private static boolean workOnMobDamage;

    private static int timeForFight;

    public void onEnable() {
        plugin = this;
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
        ptc = new PlayerTagController();

        worldGuard = new VersionWG().getWG();

        plugin.getConfig().options().copyDefaults(true);
        plugin.saveDefaultConfig();
        plugin.reloadConfig();

        if (plugin.getConfig().getBoolean("metrics")) {
            this.getLogger().info("Enabling Metrics");
            try {
                new Metrics(this, 3092);
                this.getLogger().info("Metrics loaded");
            } catch (Exception e) {
                this.getLogger().info("An error occured while trying to enable metrics. Skipping...");
            }
        }

        if (plugin.getConfig().getString("version").equals("0.1")) {
            plugin.getConfig().set("version", 0.2);
            plugin.getConfig().set("Settings.work_on_mob_damage", false);
            plugin.saveConfig();
        }

        useBossBar = plugin.getConfig().getBoolean("Settings.use_boss_bar");
        timeForFight = plugin.getConfig().getInt("Settings.time_for_fight_in_second");

        workOnMobDamage = Main.getPlugin().getConfig().getBoolean("Settings.work_on_mob_damage");

        try {
            Class.forName("org.bukkit.boss.BossBar");
        } catch (ClassNotFoundException e) {
            this.getLogger().info("The BossBar need Spigot 1.9 or more, you are in "
                    + Bukkit.getBukkitVersion());
            this.getLogger().info("BossBar feature disable");
            useBossBar = false;
        }
    }

    public void onDisable() {
        for (PlayerTag playerTag : ptc.getAll()) {
            playerTag.stopTask();
        }

    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return false;
    }

    public static PlayerTagController getPtc() {
        return ptc;
    }

    public static JavaPlugin getPlugin() {
        return plugin;
    }

    public static IWorldGuard getWorldGuard() {
        return worldGuard;
    }

    public static boolean hasWorldGuard() {
        return worldGuard != null;
    }

    public static boolean canUseBossBar() {
        return useBossBar;
    }

    public static int getTimeForFight() {
        return timeForFight;
    }

    public static void setWorkOnMobDamage(boolean value) {
        workOnMobDamage = value;
    }

    public static boolean isWorkOnMobDamage() {
        return workOnMobDamage;
    }
}
