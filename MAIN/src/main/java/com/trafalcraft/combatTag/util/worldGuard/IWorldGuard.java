package com.trafalcraft.combatTag.util.worldGuard;

import org.bukkit.entity.Player;

public interface IWorldGuard {
    public boolean pvpNotAllowed(Player p);

    public boolean mobDamageNotAllowed(Player p);
}