package com.trafalcraft.combatTag.util.worldGuard;

import com.trafalcraft.combatTag.Main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class VersionWG {

    public IWorldGuard getWG() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
        if (plugin == null) {
            Bukkit.getLogger().warning("WorldGuard hasn't been found!");
            return null;
        }
        IWorldGuard worldGuard = null;
        String wgVersion = plugin.getDescription().getVersion().split("\\.")[0];
        try {
            ClassLoader classLoader = Main.class.getClassLoader();
            classLoader.loadClass("com.trafalcraft.combatTag.util.worldGuard.WorldGuard_" + wgVersion);
            Class<?> aClass = Class.forName("com.trafalcraft.combatTag.util.worldGuard.WorldGuard_" + wgVersion);
            worldGuard = (IWorldGuard) aClass.getDeclaredConstructors()[0].newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return worldGuard;
    }
} 